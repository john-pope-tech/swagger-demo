# Full Setup Guide

This guide is the steps used to create this project. If you are looking to use this pattern in another project, or want to recreate this setup this document can serve as a reference for that.

    laravel new swagger-demo
    cd swagger-demo
    cp .env.example .env
    php artisan key:generate
    npm install

## sqlite3 (optional, needs a db backend)

    # may need php extension
    sudo apt update
    sudo apt install php-sqlite3

Create db file

    touch storage/app/db

Update .env file

    DB_CONNECTION=sqlite
    DB_DATABASE=/home/user/projects/swagger-demo/storage/app/db.sqlite

## Authentication

    php artisan make:auth

## Passport

    composer require laravel/passport
    php artisan migrate
    php artisan passport:install

> Production: use `php artisan passport:keys` instead to just create the keys, `php artisan passport:client` or the frontend to create a client

https://laravel.com/docs/5.8/passport#installation

[app/User.php](app/User.php)

    ...
    use Laravel\Passport\HasApiTokens;

    class User extends Authenticatable
    {
        use HasApiTokens, Notifiable;

        ....
    }

[app/Providers/AuthServiceProvider.php](app/Providers/AuthServiceProvider.php)

    use Laravel\Passport\Passport;
    ...

    public function boot()
    {
        ...

        Passport::routes();
    }

[config/auth.php](config/auth.php)

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],

## Passport Frontend

    php artisan vendor:publish --tag=passport-components

[resources/js/app.js](resources/js/app.js)

    Vue.component(
        'passport-clients',
        require('./components/passport/Clients.vue').default
    );

    Vue.component(
        'passport-authorized-clients',
        require('./components/passport/AuthorizedClients.vue').default
    );

    Vue.component(
        'passport-personal-access-tokens',
        require('./components/passport/PersonalAccessTokens.vue').default
    );

Compile js

    npm run dev

[resources/views/home.blade.php](resources/views/home.blade.php)

    <passport-clients></passport-clients>
    <passport-authorized-clients></passport-authorized-clients>
    <passport-personal-access-tokens></passport-personal-access-tokens>

## Swagger

    composer require darkaonline/l5-swagger
    php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"

[config/l5-swagger.php](config/l5-swagger.php)

    'api' => [
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
        'auth',
    ]

    # uncomment the 'passport' section under security

Update .env file (already in .env.example on this project)

    L5_SWAGGER_GENERATE_ALWAYS=true
    L5_SWAGGER_GENERATE_YAML_COPY=true

[app\Http\Controllers\HomeController.php](app\Http\Controllers\HomeController.php)

    /**
    * @OA\Info(title="API Demo", version="0.1")
    * @OA\Server(url="/api", description="Local server")
    */

    /**
    * @OA\Get(
    *     path="/user",
    *     @OA\Response(response="200", description="Gets the logged in user object")
    * )
    */
