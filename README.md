# Quick Start

    git clone git@bitbucket.org:john-pope-tech/swagger-demo.git
    cd swagger-demo
    cp .env.example .env
    composer install
    npm install
    php artisan key:generate

Update [.env](.env) file for database you are using

    php artisan migrate
    php artisan passport:install
    php artisan serve

* [http://localhost:8000/](http://localhost:8000/)
* create account/login
* Visit [http://localhost:8000/api/documentation](http://localhost:8000/api/documentation) to generate openapi spec files from annotations in your code:
* [storage/app/api-docs/api-docs.json](storage/app/api-docs/api-docs.json)
* [storage/app/api-docs/api-docs.yaml](storage/app/api-docs/api-docs.yaml)

Visit the [Documentation website](http://zircote.com/swagger-php/) for the [Getting started guide](http://zircote.com/swagger-php/Getting-started.html) or look at the [Examples directory](https://github.com/zircote/swagger-php/tree/master/Examples/petstore-3.0) for more examples.

## [Full Setup Guide](SETUP.md)
