<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* @OA\Info(title="API Demo", version="0.1")
* @OA\Server(url="/api", description="Local server")
*/

/**
* @OA\Get(
*     path="/user",
*     @OA\Response(response="200", description="Gets the logged in user object")
* )
*/

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
